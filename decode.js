const base45 = require('base45');
const pako = require('pako');
const cbor = require('cbor-js');
const fs = require('fs');

if(process.argv.length < 3) {
  console.log("Usage: node decode.js FILENAME");
  process.exit();
}

const schema = require("./ehn-dcc-schema/DCC.combined-schema.json");
const padding = len => Array(len + 1).join("  ");

const MAX_LENGTH = 50; // max length of description

prettyprintCert(process.argv[2]);

function prettyprintCert(filePath) {
  const dataStr = fs.readFileSync(filePath, "utf8");
  const decoded = decodeData(dataStr);
  const cert = decoded[-260][1];
  // console.log(JSON.stringify(cert, 0, 2));
  walkCert(cert);
}

function walkCert(cert, path = []) {
  if(typeof cert != "object") {
    console.log(padding(path.length) + walkSchema(schema, path, "", cert));
    return;
  }
  
  for(var key in cert) {
    var nextPath = [...path, key];
    console.log(padding(path.length) + walkSchema(schema, nextPath) + ":");
    walkCert(cert[key], nextPath);
  }
}

function walkSchema(o, _path, lastKey, value) {
  var _o = o;
  o = getReal(o);
  var path = [..._path];

  if(value && o["valueset-uri"])
    return getValue(o, value);

  if(path.length) {
    if(o.type == "array") {
      if(path.length == 1) return stringifySchema(getReal(o.items), lastKey, o);
      path.shift();
      o = getReal(o.items);
    }
    
    var key = path.shift();
    o = (o.properties || o.items)[key];
    return walkSchema(o, path, key, value);
  } else {
    return value || stringifySchema(o, lastKey, _o);
  }
}

function getValue(o, value) {
  const valueset = require("./ehn-dcc-schema/" + o["valueset-uri"]);
  return valueset.valueSetValues[value].display;
}

function stringifySchema(o, lastKey, fakeO) {
  if(o.title) return o.title;
  if(fakeO.description) return fakeO.description;
  if(o["valueset-uri"]) {
    const valueset = require("./ehn-dcc-schema/" + o["valueset-uri"]);
    return valueset.valueSetId;
  }
  if(o.description) return o.description.length > MAX_LENGTH ? o.description.substr(0, MAX_LENGTH) + "..." : o.description;
  return lastKey;
}

function getReal(o) {
  if(!o["$ref"]) return o;
  return getReal(getByRef(o["$ref"]));
}

function getByRef(refString) {
  var path = refString.split("/");
  path.shift(); // #
  var o = schema;
  while(path.length)
    o = o[path.shift()];
  return o;
}

function typedArrayToBufferSliced(array) {
  return array.buffer.slice(array.byteOffset, array.byteLength + array.byteOffset);
}

function typedArrayToBuffer(array) {
  var buffer = new ArrayBuffer(array.length);

  array.map(function (value, i) {
    return buffer[i] = value;
  })

  return array.buffer;
}

function decodeData(data) {
  if (data.startsWith('HC1')) {
    data = data.substring(3);
    if (data.startsWith(':')) {
      data = data.substring(1);
    }
  }

  var arrayBuffer = base45.decode(data);

  if (arrayBuffer[0] == 0x78) {
    arrayBuffer = pako.inflate(arrayBuffer);
  }

  var payloadArray = cbor.decode(typedArrayToBuffer(arrayBuffer));

  if (!Array.isArray(payloadArray) || payloadArray.length !== 4) {
    throw new Error('decodingFailed');
  }

  var plaintext = payloadArray[2];
  var decoded = cbor.decode(typedArrayToBufferSliced(plaintext));

  return decoded;
}
